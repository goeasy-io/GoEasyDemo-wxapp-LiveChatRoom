## 本项目已迁移至新的地址，本项目停止维护
[前往全新GoEasy聊天室/直播间弹幕源码下载和在线体验](https://www.goeasy.io/cn/demos/demos.html#livechat)


# 微信小程序快速实现聊天室（直播间） 


#### 代码量减少50%， 更简单，更清晰！

## 运行步骤

### 获取GoEasy Appkey

访问 www.goeasy.io, 注册一个账号，登录后，创建一个应用，就能得到您的appkey。

### 配置您的appkey

在app.js，将appkey替换为您自己的common key

### socket合法域名
微信小程序，需要登录到微信官方后台，将 wss://wx-hangzhou.goeasy.io加入到socket合法域名列表


### 运行

微信开发者工具运行小程序即可

### 开发者文档
[小程序集成GoEasy教程](https://docs.goeasy.io/2.x/pubsub/get-start)


## 技术支持

GoEasy官网: www.goeasy.io    扫码添加GoEasy技术为好友  
![image](https://www.goeasy.io/images/qrcode-2.jpg)


![image](./static/images/live-chatroom.gif)

